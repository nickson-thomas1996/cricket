package Cricket;
import java.time.LocalDate;
import java.util.Scanner;
public class Menu {
	public static void main(String[] args) {
		int c;
		Scanner scanner=new Scanner(System.in);
		PlayerService playerService=new PlayerServiceImpl();
		do
			{
			System.out.println("Enter choice \n 1: add \n 2: display \n 3:Search \n 4:Sort \n 5:Search By Date Of Birth \n 6:Download \n 7:Exit");
			c=scanner.nextInt();
			switch(c)
			{
			case 1:
			{    try{ 
					System.out.println("Enter the name of player");
					String name=scanner.next();
				    System.out.println("Enter the team");
				    String team=scanner.next();
				    System.out.println("Enter age");
				    int age=scanner.nextInt();
				    System.out.println("Enter date In yyyy-mm-dd format");
				    String date=scanner.next();
				    LocalDate dob=LocalDate.parse(date);
				    System.out.println("Enter Gender");
				    String gendervalue=scanner.next();
				    Gender gender=Gender.valueOf(gendervalue.toUpperCase());
				    Player player =new Player(name,team,age,dob,gender);
				    playerService.add(player);
				    }
				    catch(Exception e){
					System.out.println(e.getMessage());
					scanner.nextLine();}
			    break;
			}
			case 2:
			{
			playerService.display();
	        break;
			}
			case 3:
			{   System.out.println("Enter Name of player To Be Searched\n");
		        String name1=scanner.next();
		        playerService.search(name1);
				break;
			}
			case 4:
			{
				playerService.sort();
				break;
			}
			case 5:
			{   System.out.println("Enter Dob of player To Be Searched\n");
		        String date=scanner.next();
		        LocalDate dateOfBirth=LocalDate.parse(date);
		        playerService.search(dateOfBirth);
				break;
			}
			case 6:
			{
				playerService.download();
				break;
			}
			
			case 7:
				System.out.println("Exit");
				break;
			}}while((c!=7));
		    scanner.close();
			}
            }
