package Cricket;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public interface PlayerService {
	 void add(Player player);
	 void display();
	 void search(String name);
	 void sort();
	 void search(LocalDate dob2);
	 void download();
	 default void csvConversion(Player[] players,int j) {
		 String data;
		    StringBuffer str=new StringBuffer();
		    for(int i=0;i<j;i++)
			{ 
		    str.append("").append(players[i].getName()).append(",").append(players[i].getTeam()).append(",").append(players[i].getAge()).append(", ").append(players[i].getDob()).append(",").append(players[i].getGender()).append("\n");
			data=str.toString();
		    try {
		        FileWriter file = new FileWriter("download.csv");
		        
				file.write(data);
				file.close();
			} 
		    catch (IOException e) 
		    {
				e.printStackTrace();
			}
		    }
	} 
    }
