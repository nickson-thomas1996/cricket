package Cricket;
import java.time.LocalDate;
import java.util.Arrays;

public class PlayerServiceImpl implements PlayerService { 
	private Player []players=new Player[100];
    private int j=0;

	/**
	* The method will add a player using constructor
	* @param player
    */
	public void add(Player player)
	{   if(j==players.length)
	System.out.println("Data Limit exceeded");	
	else
    players[j]=player;
    j++; 
    }
	
	/**
	* The method will display players
	*/
	public void display()
	{   if(j==0) {
		System.out.println("No data To display");
	}else
        for(int i=0;i<j;i++)
     {
        	System.out.println(players[i]);
     }}
    
	/**
	* The method will search for players
	* @param name
	*/
	public void search(String name)
	{int flag=0;
	for(int i=0;i<j;i++)
	{  
		if(players[i].getName().equalsIgnoreCase(name))
		{
			System.out.println(players[i]);
	        flag=1;}
	}
	if(flag==0)
	System.out.println("No data To display");
	}
	
	/**
	* The method will sort players
	*/
	public void sort() 
	 {   Arrays.sort(players,0,j);
         display();
	 }
    
	/**
	* The method will Search players Using Date funtion
	* @param dob2
	*/
	public void search(LocalDate dateOfBirth) 
	    {int flag1=0;
		for(int i=0;i<j;i++)
		{
		if(players[i].getDob().isAfter(dateOfBirth)) {
		   System.out.println(players[i]);
		   flag1=1;}}
	    if(flag1==0)
		System.out.println("No data To display");}
    
	public void download() {
		csvConversion(players, j);
	    }
        }
